window.onload = () => {

    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        location.replace('/login');
    } else {
        const socket = io.connect('http://localhost:3000');
        const textForGame = document.querySelector('.text-for-game');
        const fieldForInput = document.querySelector('.field-for-input');
        const listOfPlayers = document.querySelector('.list-of-players');
        let timerForStartGame = true;

        socket.on('currentGame', currentGame => {
            const secondsToStartGame = Math.round((Date.parse(currentGame.timeStart) - Date.now()) / 1000);

            if (secondsToStartGame > 0) {
                timer(0, secondsToStartGame);
                socket.emit('join', { roomId: currentGame.roomId });
                textForGame.innerText = currentGame.text.text;
            }
            else {
                socket.emit('join', { roomId: currentGame.roomId + 1 });
            }
        })

        fieldForInput.oninput = () => {
            if (textForGame.innerText.indexOf(fieldForInput.value) == 0) {
                const progress = fieldForInput.value.length;
                const restText = textForGame.innerText.substring(progress);

                const span = document.createElement("span");
                span.classList.add("right-entered-text");
                span.innerText = textForGame.innerText.substring(0, progress);
                span.style.backgroundColor = "#7ED175";

                textForGame.innerHTML = "";
                textForGame.appendChild(span);
                textForGame.innerHTML += restText;

                const progressField = document.querySelector('.progress');
                progressField.value = progress;
                progressField.max = textForGame.innerText.length;
            }
        };

        function timer(m, s) {
            if (s < 0) {
                s = 59;
                m = m - 1;
            };
            if (m < 0) {
                handler();
                return;
            }
            document.querySelector('.time-left').innerText = `${m}m ${s}s`;
            s -= 1;
            setTimeout(() => { timer(m, s) }, 1000);
        };

        function handler() {
            if (timerForStartGame) {
                handleStartGame();
                timerForStartGame = !timerForStartGame;
            }
            else {
                handleEndGame();
            }
        }

        function handleStartGame() {
            document.querySelector('.text-for-game').style.display = "block";
            document.querySelector('.field-for-input').style.display = "block";
            document.querySelector('.progress').style.display = "block";
            timer(0, 40);
        }

        function handleEndGame() {
            const progressBar = document.querySelector('.progress');
            socket.emit('submitResult', { result: progressBar.value, maxResult: progressBar.max, token: jwt });

            socket.on('allResults', payload => {
                listOfPlayers.innerHTML = "";

                payload.users
                    .sort((a, b) => {
                        return (b.result - a.result);
                    })
                    .forEach(curUser => {
                        const { user, result, maxResult } = curUser;
                        const newLi = document.createElement('li');
                        newLi.innerHTML = `${user} `;

                        const newProgressBar = document.createElement('progress');
                        newProgressBar.value = result;
                        newProgressBar.max = maxResult;

                        newLi.appendChild(newProgressBar);
                        listOfPlayers.appendChild(newLi);
                    })

                document.querySelector('.text-for-game').style.display = "none";
                document.querySelector('.field-for-input').style.display = "none";
                document.querySelector('.progress').style.display = "none";
            });
        }

        socket.on('somebodyDisconnected', () => {
            console.log('Some user disconnected.');
        });
    }
}