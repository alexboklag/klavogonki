window.onload = () => {

    const jwt = localStorage.getItem('jwt');
    if (jwt) {
        location.replace('/klavogonki');
    } else {
        location.replace('/login');
    }
}