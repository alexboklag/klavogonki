const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');
const texts = require('./texts.json');

let n = 0;
const currentGame = {
  timeStart: false,
  roomId: 28,
  text: texts[n % 3],
  users: []
};

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/klavogonki', /*passport.authenticate('jwt'),*/ function (req, res) {
  res.sendFile(path.join(__dirname, 'klavogonki.html'));
});

app.get('/login', function (req, res) {
  res.sendFile(path.join(__dirname, 'login.html'));
});

app.post('/login', function (req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret');
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

io.on('connection', socket => {
  if (!currentGame.timeStart) {
    const timeStart = new Date();
    timeStart.setSeconds(timeStart.getSeconds() + 30);
    currentGame.timeStart = timeStart;
  }
  socket.emit('currentGame', currentGame);

  socket.on('join', ({roomId}) => {
    socket.join(roomId);
  }); 

  socket.on('submitResult', payload => {
    const { result, maxResult, token } = payload;
    const user = jwt.verify(token, 'someSecret');

    if (user) {
      const userLogin = jwt.decode(token).login;
      currentGame.users.push({user: userLogin, result, maxResult });
      socket.broadcast.to(currentGame.roomId).emit('allResults', currentGame);
      socket.emit('allResults', currentGame);
    }
  });

  socket.on('disconnect', () => {
    socket.broadcast.emit('somebodyDisconnected');
  });

});